package Supandorn.Gongphob.lab5;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class Competition {
	protected String name;
	protected LocalDate date;
	protected String place;
	protected String description;
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	public Competition(String name, String place, String date,
			String description) {
		this.name = name;
		this.place = place;
		this.date = LocalDate.parse(date,formatter);
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = LocalDate.parse(date,formatter);;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public abstract void setDescriptionAndRules();
}