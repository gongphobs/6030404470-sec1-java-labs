package Supandorn.Gongphob.lab2;

public class CoEKKUStudent {
	public static void main (String [] args) {
		if (args.length == 5) {
			String name, year ;
			double GPA, Income, ID, first_id, diff_id ;
			name = args[0];
			ID = Double.parseDouble(args[1]);
			first_id = 3415717;
			diff_id = (ID - first_id) ;
			year = args[2];
			GPA = Double.parseDouble(args[3]);
			Income = Double.parseDouble(args[4]);
			System.out.println(name + " has "+" ID = "+ String.format("%.0f",ID)+ " GPA = " + GPA + " year = " + year+" parent's income = " + Income);
			System.out.println("Your ID is different from the first CoE student's ID by "+ String.format("%.0f",diff_id)); // String.format("%.0f",) is the method to delete all decimal
		}
		else {
			System.err.println("CoEKKUStudent <name> <ID> <GPA> <academic year> <parent's income>");
			}
		}
	}
