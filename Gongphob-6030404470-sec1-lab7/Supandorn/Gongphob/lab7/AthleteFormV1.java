package Supandorn.Gongphob.lab7;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
//*this program use for input your name, birthdate,weight,height, and nationality 
public class AthleteFormV1 extends MySimpleWindow {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public AthleteFormV1(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args){
		System.out.println("HELLO");
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	public static void createAndShowGUI(){
		AthleteFormV1 athleteForm1 = new AthleteFormV1("Athlete Form V1");
		athleteForm1.addComponents();
		athleteForm1.setFrameFeatures();
	}
	
	protected void addComponents(){
		panel = new JPanel();
		
		
		//GridLayout layout = new GridLayout(0,1);
		//layout.setVgap(2);
		
		//panel.setLayout(layout);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel name_panel = new JPanel();
		JLabel name_lbl = new JLabel("Name: "); //*input name
		JTextField name_txt = new JTextField(20);
		setLabelTxtField(name_panel, name_lbl, name_txt);
		
		JPanel birth_panel = new JPanel();
		JLabel birth_lbl = new JLabel("Birthdate: "); //*input birthdate
		JTextField birth_txt = new JTextField(20);
		birth_txt.setToolTipText("ex. 22.02.2000");
		setLabelTxtField(birth_panel,birth_lbl, birth_txt);
		
		JPanel weight_panel = new JPanel();
		JLabel weight_lbl = new JLabel("Weight (kg.): "); //*input weight
		JTextField weight_txt = new JTextField(20);
		setLabelTxtField(weight_panel, weight_lbl, weight_txt);
		
		
		JPanel height_panel = new JPanel();
		JLabel height_lbl = new JLabel("height (metre):   "); //*input height
		JTextField height_txt = new JTextField(20);
		setLabelTxtField(height_panel, height_lbl, height_txt);
		
		JPanel nation_panel = new JPanel();
		JLabel nation_lbl = new JLabel("Nationality: "); //*input nationality
		JTextField nation_txt = new JTextField(20);
		setLabelTxtField(nation_panel, nation_lbl, nation_txt);
		
		JPanel ok_panel = new JPanel();
		ok_panel.add(cancel_button);
		ok_panel.add(ok_button);
		
		panel.add(ok_panel);
		
		
		
	}
	
		
	protected void setLabelTxtField(JPanel panel, JLabel label, JTextField txtField){
		panel.setLayout(new BorderLayout());
		panel.add(label,BorderLayout.WEST);
		panel.add(txtField,BorderLayout.EAST);
		
		this.panel.add(panel);
	}
	
	
}