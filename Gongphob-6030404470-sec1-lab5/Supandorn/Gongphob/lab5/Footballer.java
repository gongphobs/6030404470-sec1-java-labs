package Supandorn.Gongphob.lab5;
/**
* Footballer is a class to describe an information with the private properties as name, weight, height, gender, nationality, birthdate, position, team.
* 
*
* @author  Gongphob Supandorn
* @version 1.0
* @since   2018-02-05
*/
	public class Footballer extends Athlete{
		static String sport = "American Football";
		private String position;
		private String team;
		
		/**@param name - Firstname and Lastname
		 * @param weight-  in kilograms
		 * @param height- in meters
		 * @param gender- MALE or FEMALE
		 * @param nationality- nation of people example thai,english,American
		 * @param birthdate-Day/Month/Year
		 * @param team-team that person play with
		 * @param position- position in team
		 */
		public Footballer(String name, double weight, double height, Gender gender, String nationality, String birthdate, String position, String team) {
			super(name, weight, height, gender, nationality, birthdate);
			this.position = position;
			this.team = team;
		}

		public static String getSport() {
			return sport;
		}

		public static void setSport(String sport) {
			Footballer.sport = sport;
		}

		public String getPosition() {
			return position;
		}

		public void setPosition(String position) {
			this.position = position;
		}

		public String getTeam() {
			return team;
		}

		public void setTeam(String team) {
			this.team = team;
		}
		//Override java object to String
		public String toString() {
			return super.getName()+ ", " + super.getWeight() + "kg, " + super.getHeight() + "m, " + super.getGender()
						+ ", " + super.getNationality() + ", " + super.getBirthdate() + ", " + getSport() + ", "+getPosition()+", "+getTeam();
		}
		public void playSport() {
			System.out.println(super.getName()+" is good at "+getSport());
		}
		public void play() {
			System.out.println(super.getName()+" throws a touchdown. ");
		}
		public void move() {
			System.out.println(super.getName()+" moves down the football field. ");
		}
}

