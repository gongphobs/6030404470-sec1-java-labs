package Supandorn.Gongphob.lab4;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
/**
* Badminton is a class to describe an information with the private properties as name, weight, height, gender, nationality, birthdate, racketLength, worldRanking.
* and Compare who is older
*
* @author  Gongphob Supandorn
* @version 1.0
* @since   2018-02-05
*/
public class BadmintonPlayer extends Athlete{
	static String sport = "Badminton";
	private double racketLength;
	private int worldRanking;
	
	public BadmintonPlayer(String name, double weight, double height, Gender gender, String nationality, String birthDate, double racketLength, int worldRanking) {
		super(name, weight, height, gender, nationality, birthDate);
		this.racketLength = racketLength;
		this.worldRanking = worldRanking;
	}

	public double getRacketLength() {
		return racketLength;
	}

	public static String getSport() {
		return sport;
	}

	public static void setSport(String sport) {
		BadmintonPlayer.sport = sport;
	}

	public void setRacketLength(double racketLength) {
		this.racketLength = racketLength;
	}

	public int getWorldRanking() {
		return worldRanking;
	}

	public void setWorldRanking(int worldRanking) {
		this.worldRanking = worldRanking;
	}
	
	public void compareAge(Athlete toCompare) {
		LocalDate thisAthlete = this.getBirthdate();
		LocalDate thoseAthlete = toCompare.getBirthdate();
		int year =  Math.abs((int) ChronoUnit.YEARS.between(thisAthlete, thoseAthlete));
		if (thisAthlete.isBefore(thoseAthlete)) {
				System.out.println(this.getName()+" is "+year+" years older than "+toCompare.getName());
			}
		else {
				System.out.println(toCompare.getName()+" is "+year+" years older than "+this.getName());
			}
		}
		//Override java object to String
		public String toString() {
			return super.getName()+ ", " + super.getWeight() + "kg, " + super.getHeight() + "m, " + super.getGender()
						+ ", " + super.getNationality() + ", " + super.getBirthdate() + ", " + getSport() + ", "+getRacketLength()+", rank:"+getWorldRanking();
		}
}

