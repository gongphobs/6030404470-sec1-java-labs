package Supandorn.Gongphob.lab3;

import java.util.Scanner;
import java.util.Random;

/*How to play this game
 * this game is a game that human versus computer that human should to guest the number 1 - 6 
 * then computer will roll the dice if human'guest close to the number that computer roll
 * Human will win if human'guest now close to the random number, Computer will wins
 */
public class DiceGame {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your guest (1-6): ");
		int guestnum = sc.nextInt(); // method to accept input
		Random randnum = new Random(); // method to random
		int ans = randnum.nextInt(6) + 1; // random from 1 - 6 
		if (guestnum < 1 || guestnum > 6) { // check the input that u input if it's not correct wll show u the error
			System.err.print("Incorrect number. Only 1 - 6 can be entered.");
		} else {
			System.out.println("You have guested number " + guestnum);
			System.out.println("Computer has rolled number :" + ans);
			if (Math.abs(guestnum - ans) <=1 ) {
				System.out.println("You win.");
				System.exit(1);
			}
			else if (guestnum ==1 && ans ==6 || guestnum ==6 && ans ==1) { // check ur guest because 6,1 or 1,6 are close
				System.out.println("You win");
				System.exit(1);
			}
			else {
				System.out.println("Computer wins.");
			}
		}
		sc.close();
	}

}
