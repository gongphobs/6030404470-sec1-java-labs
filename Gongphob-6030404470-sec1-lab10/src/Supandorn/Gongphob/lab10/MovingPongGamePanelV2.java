package Supandorn.Gongphob.lab10;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import javax.swing.JPanel;

import Supandorn.Gongphob.lab10.SimpleGameWindow;
import Supandorn.Gongphob.lab10.PongPaddle;
import Supandorn.Gongphob.lab10.MovablePongPaddle;

public class MovingPongGamePanelV2 extends JPanel implements Runnable, KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected MovingBall movingBall;
	private Thread running;
	private int ballR = 20;
	protected MovablePongPaddle movableRightPad;
	protected MovablePongPaddle movableLeftPad;
	protected Rectangle2D.Double box;
	protected Integer player1Score;
	protected Integer player2Score;
	int velX = RandomVel();
	int velY = RandomVel();

	public int RandomVel() {
		Random rand = new Random();
		int Result = rand.nextInt(1 + 1) + 1;
		int xx = (int) Math.pow(-1, Result);

		return 2 * xx;
	}

	public MovingPongGamePanelV2() {
		super();
		addKeyListener(this);
		setFocusable(true);
		setBackground(Color.BLACK);

		movableLeftPad = new MovablePongPaddle(0, SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH,
				PongPaddle.HEIGHT);
		movableRightPad = new MovablePongPaddle(SimpleGameWindow.WIDTH - PongPaddle.WIDTH,
				SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH, PongPaddle.HEIGHT);

		resetBall();

		box = new Rectangle2D.Double(0, 0, SimpleGameWindow.WIDTH, SimpleGameWindow.HEIGHT);

		// set the player scores
		player1Score = 0;
		player2Score = 0;

		running = new Thread(this);
		running.start();
	}

	private void resetBall() {
		movingBall = new MovingBall(SimpleGameWindow.WIDTH / 2 - ballR, SimpleGameWindow.HEIGHT / 2 - ballR, ballR,
				velX, velY);

	}

	@Override
	public void run() {

		while (true) {

			moveBall();

			repaint();
			this.getToolkit().sync(); // to flush the graphic buffer

			// Delay
			try {
				// try to adjust the number here to have a smooth
				// running ball on your machine
				Thread.sleep(15);
			} catch (InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
		}
	}

	// update position of a ball
	private void moveBall() {

		if (movingBall.getY() < SimpleGameWindow.HEIGHT - 2 * ballR && movingBall.getY() > 0) {
			movingBall.move();
		}

		else if (movingBall.getY() >= SimpleGameWindow.HEIGHT - 2 * ballR) {
			movingBall.setVelY(-1 * movingBall.getVelY());
			movingBall.move();
		}

		else if (movingBall.getY() <= 0) {
			movingBall.setVelY(-1 * movingBall.getVelY());
			movingBall.move();
		}
		if (movingBall.intersects(movableLeftPad)) {
			movingBall.setVelX(-1 * movingBall.getVelX());
			movingBall.move();
		} else if (movingBall.intersects(movableRightPad)) {
			movingBall.setVelX(-1 * movingBall.getVelX());
			movingBall.move();
		}

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.WHITE);

		// draw the middle line
		g2.drawLine(SimpleGameWindow.WIDTH / 2, 0, SimpleGameWindow.WIDTH / 2, SimpleGameWindow.HEIGHT);

		// draw line on the left
		g2.drawLine(movableLeftPad.getW(), 0, movableLeftPad.getW(), SimpleGameWindow.HEIGHT);

		// draw line on the right
		g2.drawLine(SimpleGameWindow.WIDTH - movableRightPad.getW(), 0, SimpleGameWindow.WIDTH - movableRightPad.getW(),
				SimpleGameWindow.HEIGHT);

		// Draw the score
		g2.setFont(new Font(Font.SERIF, Font.BOLD, 48));
		g2.drawString(player1Score.toString(), SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);
		g2.drawString(player2Score.toString(), 3 * SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);

		// Draw the paddles
		g2.fill(movableLeftPad);
		g2.fill(movableRightPad);

		// draw the ball
		g2.fill(movingBall);

		// draw the box
		g2.draw(box);

	}

	@Override
	public void keyPressed(KeyEvent event) {

		int buttom = event.getKeyCode();
		if (buttom == KeyEvent.VK_UP) {
			movableRightPad.moveUp();
		} else if (buttom == KeyEvent.VK_DOWN) {
			movableRightPad.moveDown();
		} else if (buttom == KeyEvent.VK_W) {
			movableLeftPad.moveUp();
		} else if (buttom == KeyEvent.VK_S) {
			movableLeftPad.moveDown();
		}
		repaint();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

}