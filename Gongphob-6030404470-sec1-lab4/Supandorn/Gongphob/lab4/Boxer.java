package Supandorn.Gongphob.lab4;
/**
* Boxer is a class to describe an information with the private properties as name, weight, height, gender, nationality, birthdate, division, golveSize.
* 
*
* @author  Gongphob Supandorn
* @version 1.0
* @since   2018-02-05
*/
	public class Boxer extends Athlete{
		static String sport = "Boxing";
		private String division;
		private String golveSize;
		
		public Boxer(String name, double weight, double height, Gender gender, String nationality, String birthDate,String division, String golveSize ) {
			super(name, weight, height, gender, nationality, birthDate);
			this.division = division;
			this.golveSize = golveSize;
		}

		public static String getSport() {
			return sport;
		}

		public static void setSport(String sport) {
			Boxer.sport = sport;
		}

		public String getDivision() {
			return division;
		}

		public void setDivision(String division) {
			this.division = division;
		}

		public String getGolveSize() {
			return golveSize;
		}

		public void setGolveSize(String golveSize) {
			this.golveSize = golveSize;
		}
		//Override java object to String
		public String toString() {
			return super.getName()+ ", " + super.getWeight() + "kg, " + super.getHeight() + "m, " + super.getGender()
						+ ", " + super.getNationality() + ", " + super.getBirthdate() + ", " + getSport() + ", "+getDivision()+", "+getGolveSize();
		}
}
