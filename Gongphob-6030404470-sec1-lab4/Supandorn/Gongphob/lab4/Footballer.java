package Supandorn.Gongphob.lab4;
/**
* Footballer is a class to describe an information with the private properties as name, weight, height, gender, nationality, birthdate, position, team.
* 
*
* @author  Gongphob Supandorn
* @version 1.0
* @since   2018-02-05
*/
	public class Footballer extends Athlete{
		static String sport = "American Football";
		private String position;
		private String team;
		
		public Footballer(String name, double weight, double height, Gender gender, String nationality, String birthDate, String position, String team) {
			super(name, weight, height, gender, nationality, birthDate);
			this.position = position;
			this.team = team;
		}

		public static String getSport() {
			return sport;
		}

		public static void setSport(String sport) {
			Footballer.sport = sport;
		}

		public String getPosition() {
			return position;
		}

		public void setPosition(String position) {
			this.position = position;
		}

		public String getTeam() {
			return team;
		}

		public void setTeam(String team) {
			this.team = team;
		}
		//Override java object to String
		public String toString() {
			return super.getName()+ ", " + super.getWeight() + "kg, " + super.getHeight() + "m, " + super.getGender()
						+ ", " + super.getNationality() + ", " + super.getBirthdate() + ", " + getSport() + ", "+getPosition()+", "+getTeam();
		}
	}
