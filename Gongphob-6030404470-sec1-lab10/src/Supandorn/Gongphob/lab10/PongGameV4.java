package Supandorn.Gongphob.lab10;

import javax.swing.SwingUtilities;

import Supandorn.Gongphob.lab10.SimpleGameWindow;

public class PongGameV4 extends SimpleGameWindow {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PongGameV4(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		PongGameV4 window = new PongGameV4("CoE Pong Game V4");
		window.addComponents();
		window.setFrameFeatures();
	}

	private void addComponents() {
		setContentPane(new MovingPongGamePanelV3());
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}