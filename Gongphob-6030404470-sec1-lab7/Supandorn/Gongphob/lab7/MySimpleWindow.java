package Supandorn.Gongphob.lab7;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MySimpleWindow extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JButton ok_button = new JButton("OK");
	protected JButton cancel_button = new JButton("cancel");
	protected JPanel panel = new JPanel();;
	protected String title;
	protected JFrame window;
	
	

	public MySimpleWindow(String title) {
		this.title = title;
	}

	protected void addComponents() {
		

		panel.setLayout(new FlowLayout());
		panel.add(ok_button);
		panel.add(cancel_button);
	}

	protected void setFrameFeatures() {
		window = new JFrame(title);
		window.add(panel);
		window.pack();
		window.setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		window.setLocation(x, y);

	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
		}
	

	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	
	
	}
