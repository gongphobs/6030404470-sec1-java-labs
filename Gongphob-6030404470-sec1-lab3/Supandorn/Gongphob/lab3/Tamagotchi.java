package Supandorn.Gongphob.lab3;

import java.util.Scanner;

public class Tamagotchi {
	public static void main(String[] args) {
		int hour, food, water, s, feed_time, water_time;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your Tamagotchi info : ");
		int[] input = new int[3]; // to get only 3 args
		sc.close();
		for (int i = 0; i < 3; i++)// loop for check inputfrom args[0] to args[2]
		{
			input[i] = sc.nextInt();
		}
		hour = input[0];
		food = input[1];
		water = input[2];
		System.out.print("Your Tamagotchi will live for " + hour + " hours" + "\n" + "It needs to be fed every " + food
				+ " hours" + "\n" + "It needs to be watered every " + water + " hours");
		s = 0;
		// check the same feed and water times if have same times "s" will cout
		for (int i = 0; i < hour; i++) {
			if (i % food == 0 & i % water == 0) {
				s++;
			}
		}
		feed_time = food - s;
		water_time = water - s;
		System.out.println("\n\nYou need to water-feed: " + s + " times" + "\n" + "You need to water: " + water_time
				+ " times" + "\n" + "You need to feed: " + feed_time + " times");

	}

}
