package Supandorn.Gongphob.lab4;
/**
* Gender is a special class call enum to tell it has two argument
*
* @author  Gongphob Supandorn
* @version 1.0
* @since   2018-02-05
*/
	public enum Gender {
		MALE, FEMALE ;
	}

