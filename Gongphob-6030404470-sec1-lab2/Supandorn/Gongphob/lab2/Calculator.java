package Supandorn.Gongphob.lab2;

public class Calculator {
	public static void main (String[] args) {
		double operand1, operand2, ans = 0;
		String operator ;
		if (args.length != 3 && !args[2].equals(".classpath")) { 
				System.err.print("Calculator <operand1> <operand2> <operand3>");
		}
		else {
			operand1 = Double.parseDouble(args[0]);
			operand2 = Double.parseDouble(args[1]);
			operator = args[2];
			if (operator.equals("+")) {
			ans = (operand1 + operand2);
			}
			else if (operator.equals("-")) {
			ans = (operand1 - operand2);
			}
			else if  (operator.equals("*") || operator.equals(".classpath")) {
				operator = "*"; //change word'.classpath' to '*'
			ans = (operand1 * operand2);
			}
			else if(operator.equals("/")) {
				if (operand2 == 0) {
					System.out.print("The second operand cannot be zero");
					System.exit(1);
				}
				else {
					ans = (operand1 / operand2) ;
				}
		}
		System.out.print(operand1 + operator + operand2 + " = " + String.format("%.2f",ans));
	}
}
}