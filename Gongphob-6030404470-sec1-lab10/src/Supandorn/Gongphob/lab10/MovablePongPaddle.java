package Supandorn.Gongphob.lab10;

public class MovablePongPaddle extends PongPaddle{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int velocityUp;
	private int velocityDown;
	private int PAD_SPEED = 10;
	
	public MovablePongPaddle(int x, int y, int w, int h) {
		super(x, y, w, h);
	}

	public int getVelocityUp() {
		return velocityUp;
	}

	public void setVelocityUp(int velocityUp) {
		this.velocityUp = velocityUp;
	}

	public int getVelocityDown() {
		return velocityDown;
	}

	public void setVelocityDown(int velocityDown) {
		this.velocityDown = velocityDown;
	}
	
	public void moveUp(){
		if(this.y - PAD_SPEED >= 0)
			this.y-=PAD_SPEED;
	}
	
	public void moveDown() {
		if(this.y + this.height+ PAD_SPEED <= SimpleGameWindow.HEIGHT)
			this.y+=PAD_SPEED;
	}
	
}