package Supandorn.Gongphob.lab5;

public class SuperBowl extends Competition {
	private String AFCTeam;
	private String NFCTeam;
	private String winningTeam;

	public SuperBowl(String name, String place, String date, String description) {
		super(name, place, date, description);
	}

	public SuperBowl(String name, String place, String date, String description, String aFCTeam, String nFCTeam,
			String winningTeam) {
		super(name, date, place, description);
		AFCTeam = aFCTeam;
		NFCTeam = nFCTeam;
		this.winningTeam = winningTeam;
	}

	public void setDescriptionAndRules(){
		System.out.print("=== Begin: Describtion and Rules ===");
		System.out.println("\n" +super.getName() + " is played between the champions of National Football Conference (NFC)");
		System.out.println("and the American Football Conference (AFC)");
		System.out.println("the game play in four quater takes about 15 minutes");
		System.out.print("=== End: Describtion and Rules ===\n");
	}

@Override
	public String toString() {
		return  super.getName() + "(" + super.getDescription() + ") " + "was hold on " +super.getDate() +
				" in " + super.getPlace()+ "\nIt was game between"+ AFCTeam + " vs " +  NFCTeam + 
				"\nThe winner is " + winningTeam  ;
	}
}