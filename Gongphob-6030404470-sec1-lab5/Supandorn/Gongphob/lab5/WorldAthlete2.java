package Supandorn.Gongphob.lab5;

import Supandorn.Gongphob.lab5.BadmintonPlayer;

public class WorldAthlete2 {
	static boolean isTaller(Athlete A,Athlete B)  {
		double thisAthlete = A.getHeight();
		double thoseAthlete = B.getHeight();
		return thisAthlete > thoseAthlete;		
	}
	public static void main(String[] args) {
		BadmintonPlayer ratchanok = new BadmintonPlayer("Ratchanok Intanon", 
				55, 1.68, Gender.FEMALE, "Thai", "05/02/1995", 66.5,  4);
		Footballer tom = new Footballer("Tom Brady", 102, 1.93, Gender.MALE, 
				"American", "03/08/1977", "Quarterback",  "New England Patriots");
		Boxer wisaksil = new Boxer("Wisaksil Wangek", 51.5, 1.60, Gender.MALE,
				"Thai", "08/12/1986", "Super Flyweight", "M" );
		System.out.println(ratchanok);
		System.out.println(wisaksil);
		System.out.println(tom);
		BadmintonPlayer nitchaon = new BadmintonPlayer("Nitchaon Jindapol", 52, 1.63, 
				Gender.FEMALE, "Thailand", "31/03/1991", 67, 11);
		System.out.println("Both " + ratchanok.getName() + " and " + nitchaon.getName() + 
				" play " + BadmintonPlayer.getSport());
		ratchanok.compareAge(tom);
		ratchanok.compareAge(nitchaon);
		if (isTaller(wisaksil, tom)) {
			System.out.println(wisaksil.getName() + " is taller than " + tom.getName());
		}
		else {
				System.out.println(tom.getName() + " is taller than " + wisaksil.getName());
			}
		}
}