package Supandorn.Gongphob.lab5;

public class ThailandGoTournament extends Competition {
	private String winnerDesc;

	public ThailandGoTournament(String name, String place, String date) {
		super(name,date,place,null);
		this.winnerDesc = winnerDesc;
	}
	public String getWinnerDesc() {
		return winnerDesc;
	}
	public void setWinnerDesc(String winnerDesc) {
		this.winnerDesc = winnerDesc;
	}
	public void setDescriptionAndRules(){
		System.out.print("=== Begin: Describtion and Rules ===");
		System.out.println("\nThe compitition is open for all student and people");
		System.out.println("The compitition is divided into four categories");
		System.out.println("1. High Dan (3 Dan and above)");
		System.out.println("2. Low Dan (1-2 Dan)");
		System.out.println("3. High Kyu (1-4 Kyu)");
		System.out.println("4. Low Kyu (5-8 Kyu)");
		System.out.print("=== End: Describtion and Rules ===\n");
	}

	@Override
	public String toString() {
		return super.getName() + " was hold on " +super.getDate() +
				" in " + super.getPlace() + "\nSome winner are " + winnerDesc;
	}
	
}