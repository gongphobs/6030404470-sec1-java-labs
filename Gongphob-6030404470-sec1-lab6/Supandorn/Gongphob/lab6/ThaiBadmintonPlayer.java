package Supandorn.Gongphob.lab6;
import Supandorn.Gongphob.lab5.Gender;
public class ThaiBadmintonPlayer extends Supandorn.Gongphob.lab5.BadmintonPlayer {
	private static String nationality = "thai";
	protected String equipment = "�١����";

	public ThaiBadmintonPlayer(String name, double weight, double height, Gender gender ,
			String birthdate, double racketLength, int worldRanking) {
		super(name, weight, height, gender, nationality, birthdate, racketLength, worldRanking);
		this.nationality = nationality;
		// TODO Auto-generated constructor stub
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	public void play() {
		super.play();
		System.out.println(super.getName()+" hits "+equipment);
	}
}