package Supandorn.Gongphob.lab9;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import javax.swing.event.MenuKeyListener;

public class AthleteFormV5 extends AthleteFormV4 implements ActionListener,ItemListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JMenuItem blue,green,red,custom_c;
	protected JMenuItem s16,s20,s24,custom_s;

	public AthleteFormV5(String title) {
		super(title);
	}	
	public static void createAndShowGUI(){
		AthleteFormV5 msw = new AthleteFormV5("AthleteFormV5");
		msw.addComponents();
		msw.addMenus();
		msw.setFrameFeatures();
		msw.addListeners();
		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	protected void addMenus () {
		super.addMenus();
		colorMI = new JMenu("Color");
		sizeMI = new JMenu("Size");
		blue = new JMenuItem("Blue");
		colorMI.add(blue);
		green = new JMenuItem("Green");
		colorMI.add(green);
		red = new JMenuItem("Red");
		colorMI.add(red);
		custom_c = new JMenuItem("Custom..");
		colorMI.add(custom_c);
		
		s16 = new JMenuItem("16");
		sizeMI.add(s16);
		s20 = new JMenuItem("20");
		sizeMI.add(s20);
		s24 = new JMenuItem("24");
		sizeMI.add(s24);
		custom_s = new JMenuItem("Custom..");
		sizeMI.add(custom_s);
		
	}
	protected void addListeners() {
		super.addListeners();
		sizeMI.addMenuKeyListener((MenuKeyListener) this);
		colorMI.addMenuKeyListener((MenuKeyListener) this);
		blue.addActionListener(this);
		green.addActionListener(this);
		red.addActionListener(this);
		s16.addActionListener(this);
		s20.addActionListener(this);
		s24.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src3 = event.getSource();
		if (src3.equals(s16)){
			size16();
		}else if (src3.equals(s20)){
			size20();
		}else if (src3.equals(s24)){
			size24();
		}else if (src3.equals(blue)){
			blueColor();
		}else if (src3.equals(green)){
			greenColor();
		}else if (src3.equals(red)){
			redColor();
		}
		
		
	}
	private void blueColor() {
		nameTxtField.setForeground(Color.BLUE);
		dateTxtField.setForeground(Color.BLUE);
		weightTxtField.setForeground(Color.BLUE);
		heightTxtField.setForeground(Color.BLUE);
		nationalityTxtField.setForeground(Color.BLUE);
		compTxtArea.setForeground(Color.BLUE);
		
	}
	
	private void greenColor() {
		nameTxtField.setForeground(Color.GREEN);
		dateTxtField.setForeground(Color.GREEN);
		weightTxtField.setForeground(Color.GREEN);
		heightTxtField.setForeground(Color.GREEN);
		nationalityTxtField.setForeground(Color.GREEN);
		compTxtArea.setForeground(Color.GREEN);
		
	}
	
	private void redColor() {
		nameTxtField.setForeground(Color.RED);
		dateTxtField.setForeground(Color.RED);
		weightTxtField.setForeground(Color.RED);
		heightTxtField.setForeground(Color.RED);
		nationalityTxtField.setForeground(Color.RED);
		compTxtArea.setForeground(Color.RED);
		
	}
	private void size16(){
		nameTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,16));
		dateTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,16));
		weightTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,16));
		heightTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,16));
		nationalityTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,16));
		compTxtArea.setFont(new Font("Sans Serif" ,Font.BOLD,16));
	}
	private void size20(){
		nameTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,20));
		dateTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,20));
		weightTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,20));
		heightTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,20));
		nationalityTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,20));
		compTxtArea.setFont(new Font("Sans Serif" ,Font.BOLD,20));
	}
	private void size24(){
		nameTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,24));
		dateTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,24));
		weightTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,24));
		heightTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,24));
		nationalityTxtField.setFont(new Font("Sans Serif" ,Font.BOLD,24));
		compTxtArea.setFont(new Font("Sans Serif" ,Font.BOLD,24));
	}
	
}
