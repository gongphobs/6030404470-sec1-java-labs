package Supandorn.Gongphob.lab5;
/**
* Gender is a special class(enum) that contain two argument call MALE, FEMALE
*
* @author  Gongphob Supandorn
* @version 1.0
* @since   2018-02-05
*/
	public enum Gender {
		MALE, FEMALE ;
	}

