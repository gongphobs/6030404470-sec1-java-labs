package Supandorn.Gongphob.lab7;
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
//*do the same thing AthleteFormV2 just add type so you can choose it
public class AthleteFormV3 extends AthleteFormV2 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AthleteFormV3(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public static void createAndShowGUI(){
		AthleteFormV3 AthleteForm3 = new AthleteFormV3("Athlete Form V3");
		AthleteForm3.addComponents();
		AthleteForm3.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	
	@Override
	protected void addComponents() {
		// TODO Auto-generated method stub
		super.addComponents();
		pack();
		JComboBox type = new JComboBox();
		type.addItem("Badminton player");
		type.addItem("Boxer");
		type.addItem("Footballer");
		type.setSelectedIndex(1);
		JPanel combo_panel = new JPanel();
		JLabel ath_lbl = new JLabel("Type: ");
		combo_panel.setLayout(new BorderLayout());
		combo_panel.add(ath_lbl,BorderLayout.WEST);
		combo_panel.add(type,BorderLayout.EAST);
		panel.add(combo_panel);
		
		JPanel ok_panel = new JPanel();
		ok_panel.add(cancel_button);
		ok_panel.add(ok_button);
		panel.add(ok_panel);
		
	}
	
	@Override
	protected void setFrameFeatures() {
		// TODO Auto-generated method stub
		super.setFrameFeatures();
		
		JMenuBar menubar = new JMenuBar();
		
		JMenu file = new JMenu("File");
		JMenuItem item_new = new JMenuItem("New");
		JMenuItem item_open = new JMenuItem("Open");
		JMenuItem item_save = new JMenuItem("Save");
		JMenuItem item_exit = new JMenuItem("Exit");
		
		file.add(item_new);
		file.add(item_open);
		file.add(item_save);
		file.add(item_exit);
		
		JMenu config = new JMenu("Config");
		JMenuItem color = new JMenuItem("Color");
		JMenuItem size = new JMenuItem("Size");
		
		config.add(color);
		config.add(size);
		
		
		menubar.add(file);
		menubar.add(config);
		
		window.setJMenuBar(menubar);
		
		
	}
}
