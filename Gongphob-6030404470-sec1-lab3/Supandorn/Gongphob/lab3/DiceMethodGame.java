package Supandorn.Gongphob.lab3;

import java.util.Scanner;
import java.util.Random;

public class DiceMethodGame {
	static int humanGuest;
	static int computerScore;
	
	public static int acceptInput() { //method to get input
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your guest (1-6) : ");
		int humanGuest = sc.nextInt();
		sc.close();
		return humanGuest ;
	}
	public static int genDiceroll() { // method to random the dice
		Random randomdice = new Random();
		int computerScore = randomdice.nextInt(6) + 1;
		return computerScore;
	}
	public static void DisplayWinner(int humanGuest, int computerScore) { // method to check who is the winner
		if (humanGuest < 1 || humanGuest > 6) {
			System.err.println("Incorrect number. Only 1 - 6 can be entered.");
		}
		else if (Math.abs(humanGuest - computerScore) <=1 || Math.abs(humanGuest - computerScore) ==5 ) { // check the dice's number if they are close together
			System.out.println("You win.");
			System.exit(1);
		}
		else {
			System.out.println("Computer wins.");
		}
	}
}
	

