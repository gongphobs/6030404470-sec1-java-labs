package Supandorn.Gongphob.lab10;

import javax.swing.SwingUtilities;

public class PongGameV2 extends SimpleGameWindow{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PongGameV2(String title) {
		super(title);
	}
	
	public static void createAndShowGUI() {
		PongGameV2 window = new PongGameV2("CoE Pong Game V2");
		window.addComponents();
		window.setFrameFeatures();
	}

	private void addComponents() {
		setContentPane(new MovingPongGamePanel());
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}