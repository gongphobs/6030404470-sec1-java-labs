package Supandorn.Gongphob.lab9;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import Supandorn.Gongphob.lab7.AthleteFormV3;
import Supandorn.Gongphob.lab7.MySimpleWindow;

public class AthleteFormV4 extends AthleteFormV3 implements ActionListener,ItemListener {
	private static final long serialVersionUID = 1L;
	protected String male_gen,female_gen,baddialog,boxdialog,footdialog,selectedtype;
	protected JOptionPane message,Pane_male,Pane_female,Pane_Bad,Pane_Box,Pane_Foot;
	protected JDialog jdmale,jdfemale;
	public AthleteFormV4(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		Object src2 = event.getSource();
		if (src == okButton) {
			handleOKButton();
		} else if (src == cancelButton) {
			handleCancelButton();
		}
		else if(src2 == maleRadioB){
	    	 maleDialog();}
	       else if (src2 == femaleRadioB) {
	    	   femaleDialog();}

		// TODO Auto-generated method stub
	}
	protected void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		femaleRadioB.addActionListener(this);
		maleRadioB.addActionListener(this);
		typeList.addItemListener(this);

	}
	private void handleCancelButton() {
		nameTxtField.setText("");
		dateTxtField.setText("");
		weightTxtField.setText("");
		heightTxtField.setText("");
		nationalityTxtField.setText("");
		compTxtArea.setText("");
		
	}
	@Override
	public void itemStateChanged(ItemEvent event2) {
		String selectedtype = (String) typeList.getSelectedItem();
		if (selectedtype.equals("Badminton player")){
  		  BadmintonCombobox();}
		else if (selectedtype.equals("Boxer")){
			BoxingCombobox();}
		else if (selectedtype.equals("Footballer")){
  		 FootballCombobox();}
	}
	
	private void handleOKButton() {
		message = new JOptionPane();
		String showGender ;
		if (maleRadioB.isSelected()) {
			showGender =  "\nGender = " + maleRadioB.getText();
		}
		else {
			showGender =  "\nGender = " +femaleRadioB.getText();	
			}
		String show = " Name = " + nameTxtField.getText() + ",Brithdate = " + dateTxtField.getText() + ",Weight = " + weightTxtField.getText()
		+ ",Height = " + heightTxtField.getText() + ",Nationality = " + nationalityTxtField.getText() ;
		String showV1 = show + showGender + "\nCompetition = " + compTxtArea.getText() + "\nType = " + typeList.getSelectedItem();
		
		JOptionPane.showMessageDialog(message, showV1);
	}
	public static void createAndShowGUI(){
		AthleteFormV4 msw = new AthleteFormV4("AthleteFormV4");
		msw.addComponents();
		msw.setFrameFeatures();
		msw.addListeners();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	private void maleDialog(){
		male_gen = "Your gender type is now changed to Male";
		Pane_male = new JOptionPane(male_gen);
		jdmale = Pane_male.createDialog(Pane_male,male_gen);
		jdmale.setTitle("Gender info");
		jdmale.setLocation(getX(),(getY()+(getY()+getY()/3+20)));
        jdmale.setVisible(true);
		
	}
	private void femaleDialog(){
		female_gen = "Your gender type is now changed to Female";
		Pane_female = new JOptionPane(female_gen);
		jdfemale = Pane_female.createDialog(Pane_female,female_gen);
		jdfemale.setTitle("Gender info");
		jdfemale.setLocation(getX(),getY()+(getY()+getY()/3+20));
        jdfemale.setVisible(true);

	}
	private void BadmintonCombobox(){
		baddialog = "Your athele type is changed to Badminton player";
		Pane_Bad = new JOptionPane();
		JOptionPane.showMessageDialog(Pane_Bad,baddialog);
	}
	private void BoxingCombobox(){
		boxdialog = "Your athele type is changed to Boxer";
		Pane_Box = new JOptionPane();
		JOptionPane.showMessageDialog(Pane_Box,boxdialog);
	}
	private void FootballCombobox(){
		footdialog = "Your athele type is changed to Footballer";
		Pane_Foot = new JOptionPane();
		JOptionPane.showMessageDialog(Pane_Foot,footdialog);
	}
	

}


