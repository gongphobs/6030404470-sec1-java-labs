package Supandorn.Gongphob.lab7;
import java.awt.BorderLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

//*do the same thing AthleteFormV1 just add gender
public class AthleteFormV2 extends AthleteFormV1 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public AthleteFormV2(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	
	public static void createAndShowGUI(){
		AthleteFormV2 AthleteForm2 = new AthleteFormV2("Athlete Form V2");
		AthleteForm2.addComponents();
		AthleteForm2.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	
	@Override
	protected void addComponents() {
		// TODO Auto-generated method stub
		super.addComponents();
		
		JLabel gender_lbl = new JLabel("Gender: "); 
		JRadioButton male = new JRadioButton("Male",true); //*you are male
		JRadioButton female = new JRadioButton("Female");
		ButtonGroup group = new ButtonGroup();
		group.add(male);
		group.add(female);
		
		
		JPanel gender_panel = new JPanel();
		JPanel checkbox_panel = new JPanel();
		
		checkbox_panel.add(male);
		checkbox_panel.add(female);
		
		gender_panel.setLayout(new BorderLayout());
		gender_panel.add(gender_lbl,BorderLayout.WEST);
		gender_panel.add(checkbox_panel,BorderLayout.EAST);
		panel.add(gender_panel);
		
		JPanel compet_panel = new JPanel();
		compet_panel.setLayout(new BorderLayout());
		JLabel compet_lbl = new JLabel("Compettion: ");
		compet_panel.add(compet_lbl,BorderLayout.WEST);
		
		panel.add(compet_panel);
		
		JTextArea ta = new JTextArea(2,35);
		ta.setText("Competed in the 31st national championship");
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		JScrollPane scr_pane = new JScrollPane(ta);
		
		panel.add(scr_pane);
		
		
		JPanel ok_panel = new JPanel();
		ok_panel.add(cancel_button);
		ok_panel.add(ok_button);
		panel.add(ok_panel);
		
		
		
		
	}
}
