package Supandorn.Gongphob.lab9;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AthleteFormV6 extends AthleteFormV5{
	protected ImageIcon lanla;
	protected JLabel imageJL;
	protected JPanel imagePL,extraPanel,imagePanel;
	private static final long serialVersionUID = 1L;
	
	public AthleteFormV6(String title) {
		super(title);
	}
	public static void createAndShowGUI(){
		AthleteFormV6 msw = new AthleteFormV6("AthleteFormV6");
		msw.addComponents();
		msw.addMenus();
		msw.setFrameFeatures();
		msw.addListeners();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	protected void addListeners() {
		super.addListeners();
	}
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
	}
	protected void addMenus () {
		super.addMenus();
		super.openMI.setIcon(new ImageIcon("images/openIcon.png"));
		super.saveMI.setIcon(new ImageIcon("images/saveIcon.png"));
		super.exitMI.setIcon(new ImageIcon("images/quitIcon.png"));
		
	} 
	public void addComponents(){
		super.addComponents();
		lanla = new ImageIcon("images/lanla.jpg");
		imagePL =  new JPanel();
		imagePanel = new JPanel(new BorderLayout());
		extraPanel = new JPanel(new BorderLayout());
		imageJL = new JLabel(lanla);
		imagePL.add(imageJL);
		imagePanel.add(imagePL, BorderLayout.NORTH);
		extraPanel.add(textsPanel, BorderLayout.NORTH);
		extraPanel.add(genderPanel, BorderLayout.CENTER);
		extraPanel.add(compPanel, BorderLayout.SOUTH);
		overallPanel.add(imagePanel, BorderLayout.NORTH);
		overallPanel.add(extraPanel, BorderLayout.CENTER);
	}
}
