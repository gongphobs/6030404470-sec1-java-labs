package Supandorn.Gongphob.lab5;

import Supandorn.Gongphob.lab5.BadmintonPlayer;

public class WorldAthlete3 {
	public static void main(String[] args) {
	    Athlete ratchanok = new BadmintonPlayer("Ratchanok Intanon", 
	            55, 1.68, Gender.FEMALE, "Thai", "05/02/1995", 66.5,  4);
	    Footballer tom = new Footballer("Tom Brady", 102, 1.93, Gender.MALE, 
	            "American", "03/08/1977", "Quarterback",  "New England Patriots");
	    Athlete wisaksil = new Athlete("Wisaksil Wangek", 51.5, 1.60, Gender.MALE,
	            "Thai", "08/12/1986");
	    System.out.println(ratchanok);
	    System.out.println(tom);
	    System.out.println(wisaksil);
	    
	    ((BadmintonPlayer)ratchanok).setWorldRanking(3);
	    System.out.println(ratchanok.getName() + " has moved up to "
	            + "world ranking number " + ((BadmintonPlayer)ratchanok).getWorldRanking());
	    
	    tom.setTeam("San Francisco 49ers");
	    System.out.println(tom.getName() + " has moved to new team " + tom.getTeam());
	    
	    ratchanok.playSport();
	    tom.playSport();
	    wisaksil.playSport();
	    
	    BadmintonPlayer tai = new BadmintonPlayer("Tai Tzu-Ying", 
	            57, 1.62, Gender.FEMALE, "Taiwan", "20/06/1994", 67.0,  1);
	    
	    BadmintonPlayer.setSport("Gymnastic");
	    System.out.println(ratchanok.getName() + " and " + tai.getName() + 
	            " have changed to play " + BadmintonPlayer.getSport());
	}
}
