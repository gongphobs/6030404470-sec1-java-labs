package Supandorn.Gongphob.lab10;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import javax.swing.JPanel;

import Supandorn.Gongphob.lab10.SimpleGameWindow;
import Supandorn.Gongphob.lab10.PongPaddle;
import Supandorn.Gongphob.lab10.MovablePongPaddle;
import Supandorn.Gongphob.lab10.MovingBall;

public class MovingPongGamePanelV3 extends JPanel implements Runnable, KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected MovingBall movingBall;
	private Thread running;
	private int ballR = 20;
	protected MovablePongPaddle movableRightPad;
	protected MovablePongPaddle movableLeftPad;
	protected Rectangle2D.Double box;
	protected Integer player1Score;
	protected Integer player2Score;

	int score = 10;
	int velX = RandomVel();
	int velY = RandomVel();
	int very = 3;
	int little = -3;

	public int RandomVel() {

		Random r1 = new Random();
		int ans1 = r1.nextInt(1 + 1) + 1;
		int result = (int) Math.pow(-1, ans1);
		Random r2 = new Random();
		int ans2 = r2.nextInt() + 1;
		System.out.println(ans2 * result);

		return ans2 * result;
	}

	public MovingPongGamePanelV3() {
		super();

		addKeyListener(this);
		setFocusable(true);

		setBackground(Color.BLACK);

		// initialize pads
		movableLeftPad = new MovablePongPaddle(0, SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH,
				PongPaddle.HEIGHT);
		movableRightPad = new MovablePongPaddle(SimpleGameWindow.WIDTH - PongPaddle.WIDTH,
				SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH, PongPaddle.HEIGHT);
		resetBall();

		// initialize the ball
		box = new Rectangle2D.Double(0, 0, SimpleGameWindow.WIDTH, SimpleGameWindow.HEIGHT);

		// set the player scores
		player1Score = 0;
		player2Score = 0;

		running = new Thread(this);
		running.start();
	}

	private void resetBall() {
		movingBall = new MovingBall(SimpleGameWindow.WIDTH / 2 - ballR, SimpleGameWindow.HEIGHT / 2 - ballR, ballR,
				velX, velY);

	}

	@Override
	public void run() {

		while (true) {

			moveBall();
			repaint();
			this.getToolkit().sync(); // to flush the graphic buffer

			// Delay
			try {
				// try to adjust the number here to have a smooth
				// running ball on your machine
				Thread.sleep(15);
			} catch (InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
		}
	}

	// update position of the ball
	private void moveBall() {

		if (movingBall.getY() < SimpleGameWindow.HEIGHT - 2 * ballR && movingBall.getY() > 0) {
			movingBall.move();
		}

		else if (movingBall.getY() >= SimpleGameWindow.HEIGHT - 2 * ballR) {
			movingBall.setVelY(-1 * movingBall.getVelY());
			movingBall.move();
		}

		else if (movingBall.getY() <= 0) {
			movingBall.setVelY(-1 * movingBall.getVelY());
			movingBall.move();
		} else if (movingBall.getX() == PongPaddle.WIDTH
				&& movingBall.getY() <= movableLeftPad.getY() + PongPaddle.HEIGHT
				&& movingBall.getY() >= movableLeftPad.getY()) {
			movingBall.setVelX(-1 * movingBall.getVelX());
			movingBall.move();
		} else if (movingBall.getX() == SimpleGameWindow.WIDTH - PongPaddle.WIDTH - 2 * ballR
				&& movingBall.getY() <= movableRightPad.getY() + PongPaddle.HEIGHT
				&& movingBall.getY() >= movableRightPad.getY()) {
			movingBall.setVelX(-1 * movingBall.getVelX());
			movingBall.move();
		}
		if (movingBall.getX() > SimpleGameWindow.WIDTH + 2 * ballR) {
			player1Score += 1;
			velX = RandomVel();
			velY = RandomVel();
			resetBall();
		} else if (movingBall.getX() < 0 - 4 * ballR) {
			player2Score += 1;
			velX = RandomVel();
			velY = RandomVel();
			resetBall();

		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.WHITE);

		// draw the middle line
		g2.drawLine(SimpleGameWindow.WIDTH / 2, 0, SimpleGameWindow.WIDTH / 2, SimpleGameWindow.HEIGHT);

		// draw line on the left
		g2.drawLine(movableLeftPad.getW(), 0, movableLeftPad.getW(), SimpleGameWindow.HEIGHT);

		// draw line on the right
		g2.drawLine(SimpleGameWindow.WIDTH - movableRightPad.getW(), 0, SimpleGameWindow.WIDTH - movableRightPad.getW(),
				SimpleGameWindow.HEIGHT);

		// Draw the score
		g2.setFont(new Font(Font.SERIF, Font.BOLD, 48));
		g2.drawString(player1Score.toString(), SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);
		g2.drawString(player2Score.toString(), 3 * SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);

		if (player2Score == score) {
			resetBall();
			g2.drawString("Player 2 wins", SimpleGameWindow.WIDTH / 4 + 25, 3 * SimpleGameWindow.HEIGHT / 10);
		} else if (player1Score == score) {
			resetBall();
			g2.drawString("Player 1 wins", SimpleGameWindow.WIDTH / 4 + 25, 3 * SimpleGameWindow.HEIGHT / 10);
		}

		// Draw the paddles
		g2.fill(movableLeftPad);
		g2.fill(movableRightPad);

		// draw the ball
		g2.fill(movingBall);

		// draw the box
		g2.draw(box);

	}

	@Override
	public void keyPressed(KeyEvent event) {
		int key = event.getKeyCode();
		if (key == KeyEvent.VK_UP) {
			movableRightPad.moveUp();
		} else if (key == KeyEvent.VK_DOWN) {
			movableRightPad.moveDown();
		} else if (key == KeyEvent.VK_W) {
			movableLeftPad.moveUp();
		} else if (key == KeyEvent.VK_S) {
			movableLeftPad.moveDown();
		}
		repaint();

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}
}