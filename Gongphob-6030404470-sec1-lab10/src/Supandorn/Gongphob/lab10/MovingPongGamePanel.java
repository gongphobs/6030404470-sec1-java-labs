package Supandorn.Gongphob.lab10;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

public class MovingPongGamePanel extends JPanel implements KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Integer player1Score;
	protected Integer player2Score;
	protected Ball ball;
	private int ballR = 20; // radius of the ball
	protected Rectangle2D.Double box;
	protected MovablePongPaddle movableLeftPad;
	protected MovablePongPaddle movableRightPad;
	final static int moveUp = 1 , moveDown = -1, Stop = 0 ;
	protected int leftPadMove = 0 , rightPadMove = 0 ;

	public MovingPongGamePanel() {
		super();

		addKeyListener(this);
		setFocusable(true);

		setBackground(Color.BLACK);

		// initialize the pads
		movableLeftPad = new MovablePongPaddle(0, SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH,
				PongPaddle.HEIGHT);
		movableRightPad = new MovablePongPaddle(SimpleGameWindow.WIDTH - PongPaddle.WIDTH,
				SimpleGameWindow.HEIGHT / 2 - PongPaddle.HEIGHT / 2, PongPaddle.WIDTH, PongPaddle.HEIGHT);

		// initialize the ball
		ball = new Ball(SimpleGameWindow.WIDTH / 2 - ballR, SimpleGameWindow.HEIGHT / 2 - ballR, ballR);

		// initialize the ball
		box = new Rectangle2D.Double(0, 0, SimpleGameWindow.WIDTH, SimpleGameWindow.HEIGHT);

		// set the player scores
		player1Score = 0;
		player2Score = 0;

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if (leftPadMove == 1) {
			movableLeftPad.moveUp();
		}

		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.WHITE);

		// draw the middle line
		g2.drawLine(SimpleGameWindow.WIDTH / 2, 0, SimpleGameWindow.WIDTH / 2, SimpleGameWindow.HEIGHT);

		// draw line on the left
		g2.drawLine(movableLeftPad.getW(), 0, movableLeftPad.getW(), SimpleGameWindow.HEIGHT);

		// draw line on the right
		g2.drawLine(SimpleGameWindow.WIDTH - movableRightPad.getW(), 0, SimpleGameWindow.WIDTH - movableRightPad.getW(),
				SimpleGameWindow.HEIGHT);

		// Draw the score
		g2.setFont(new Font(Font.SERIF, Font.BOLD, 48));
		g2.drawString(player1Score.toString(), SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);
		g2.drawString(player2Score.toString(), 3 * SimpleGameWindow.WIDTH / 4, SimpleGameWindow.HEIGHT / 5);

		// Draw the paddles
		g2.fill(movableLeftPad);
		g2.fill(movableRightPad);
		
		// draw the ball
		g2.fill(ball);

		// draw the box
		g2.draw(box);

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		int keyCode = e.getKeyCode();
		if (keyCode == KeyEvent.VK_UP) {
			rightPadMove = moveUp;
		}
		else if (keyCode == KeyEvent.VK_W) {
			leftPadMove = moveUp;
		}
		else if (keyCode == KeyEvent.VK_DOWN) {
			rightPadMove = moveDown;
		}
		else if (keyCode == KeyEvent.VK_S) {
			leftPadMove = moveDown;
		}
		repaint();
			
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}