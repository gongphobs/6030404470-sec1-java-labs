package Supandorn.Gongphob.lab5;
/**
* Boxer is a class to describe an information with the private properties as name, weight, height, gender, nationality, birthdate, division, golveSize.
* 
*
* @author  Gongphob Supandorn
* @version 1.0
* @since   2018-02-05
*/
	public class Boxer extends Athlete{
		static String sport = "Boxing";
		private String division;
		private String golveSize;
		
		/**@param name - Firstname and Lastname
		 * @param weight- in kilograms
		 * @param height- in meters
		 * @param gender- MALE or FEMALE
		 * @param nationality - nation of people example thai,english,American
		 * @param birthdate - Day/Month/Year
		 * @param division - divistion that person fight
		 * @param golveSize - size of glove
		 */
		public Boxer(String name, double weight, double height, Gender gender, String nationality, String birthdate,String division, String golveSize ) {
			super(name, weight, height, gender, nationality, birthdate);
			this.division = division;
			this.golveSize = golveSize;
		}

		public static String getSport() {
			return sport;
		}

		public static void setSport(String sport) {
			Boxer.sport = sport;
		}

		public String getDivision() {
			return division;
		}

		public void setDivision(String division) {
			this.division = division;
		}

		public String getGolveSize() {
			return golveSize;
		}

		public void setGolveSize(String golveSize) {
			this.golveSize = golveSize;
		}
		//Override java object to String
		public String toString() {
			return super.getName()+ ", " + super.getWeight() + "kg, " + super.getHeight() + "m, " + super.getGender()
						+ ", " + super.getNationality() + ", " + super.getBirthdate() + ", " + getSport() + ", "+getDivision()+", "+getGolveSize();
		}
		public void playSport() {
			System.out.println(super.getName()+" is good at "+getSport());
		}
		public void play() {
			System.out.println(super.getName()+" throws a punch. ");
		}
		public void move() {
			System.out.println(super.getName()+" moves around a boxing ring. ");
		}
}

