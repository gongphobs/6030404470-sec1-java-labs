package Supandorn.Gongphob.lab2;
public class Substringchecker {
    public static void main(String[] args){
    if(args.length>=2)
    {
        int i = 0;  /*count string in loop*/
        int n = 0; /*count matching string in loop*/
        int num = args.length-1;
        while(i < num) {/*checking all string*/
            if(args[i+1].contains(args[0])){ /*when there is string that contains the substring*/
                i += 1;
                n += 1;
                System.out.println("String "+ i +" : " +args[i] + " contains " + args[0]);
            }
            else{i+=1;}/*counting*/
            
            }
        if (n==0) {/*when there are no strings that contain the substring*/
            System.out.println("There are no strings that contain "+args[0]);
        }
        else{
            System.out.println("There are "+ n +" strings that contain " + args[0]);
        }
    }
    else{/*when there is one string that contains the substring*/
        System.err.println("SubstringChecker <substring> <str1> ...");
    }
	}
}
